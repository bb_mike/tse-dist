webpackJsonp([1,8],{

/***/ 1071:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_shared_module__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__career_site_jobs_component__ = __webpack_require__(1076);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__job_locations_job_locations_component__ = __webpack_require__(1077);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__job_posting_data_job_posting_data_component__ = __webpack_require__(1078);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__top_job_categories_top_job_categories_component__ = __webpack_require__(1079);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__career_site_jobs_routing__ = __webpack_require__(1091);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_google_maps_core__ = __webpack_require__(485);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_google_maps_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_angular2_google_maps_core__);
/* harmony export (binding) */ __webpack_require__.d(exports, "CareerSiteJobsModule", function() { return CareerSiteJobsModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CareerSiteJobsModule = (function () {
    function CareerSiteJobsModule() {
    }
    CareerSiteJobsModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_6__career_site_jobs_routing__["a" /* routing */],
                __WEBPACK_IMPORTED_MODULE_7_angular2_google_maps_core__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyAj8pAKZS2gQ9D7s67f9G_ImAL6vmJOEPg'
                })
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__career_site_jobs_component__["a" /* CareerSiteJobsComponent */], __WEBPACK_IMPORTED_MODULE_3__job_locations_job_locations_component__["a" /* JobLocationsComponent */], __WEBPACK_IMPORTED_MODULE_4__job_posting_data_job_posting_data_component__["a" /* JobPostingDataComponent */], __WEBPACK_IMPORTED_MODULE_5__top_job_categories_top_job_categories_component__["a" /* TopJobCategoriesComponent */]]
        }), 
        __metadata('design:paramtypes', [])
    ], CareerSiteJobsModule);
    return CareerSiteJobsModule;
}());


/***/ },

/***/ 1076:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return CareerSiteJobsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CareerSiteJobsComponent = (function () {
    function CareerSiteJobsComponent() {
        this.whichReport = 'job-locations';
    }
    CareerSiteJobsComponent.prototype.moveSlider = function () {
        if (this.whichReport === 'job-locations') {
            this.whichReport = 'job-posting-data';
        }
        else {
            this.whichReport = 'top-job-categories';
        }
    };
    CareerSiteJobsComponent.prototype.ngOnInit = function () {
    };
    CareerSiteJobsComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-career-site-jobs',
            template: __webpack_require__(1111),
            styles: [__webpack_require__(1096)],
            animations: [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('reportState', [
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('job-locations', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                        left: '0px',
                        width: '80px'
                    })),
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('job-posting-data', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                        left: '100px',
                        width: '110px'
                    })),
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('top-job-categories', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                        left: '230px',
                        width: '120px'
                    })),
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('job-locations <=> job-posting-data', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('400ms')),
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('job-locations <=> top-job-categories', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('400ms')),
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('job-posting-data <=> job-locations', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('400ms')),
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('job-posting-data <=> top-job-categories', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('400ms')),
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('top-job-categories <=> job-locations', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('400ms')),
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('top-job-categories <=> job-posting-data', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('400ms'))
                ])
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], CareerSiteJobsComponent);
    return CareerSiteJobsComponent;
}());


/***/ },

/***/ 1077:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__tse_service__ = __webpack_require__(95);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return JobLocationsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var JobLocationsComponent = (function () {
    function JobLocationsComponent(locationsService) {
        this.locationsService = locationsService;
        this.locationsArray = [];
        // Style array for the map
        this.stylesArray = [
            {
                featureType: 'administrative',
                elementType: 'labels.text',
                stylers: [
                    { color: '#ccdbeb' },
                ]
            },
            {
                featureType: 'administrative',
                elementType: 'labels.text.stroke',
                stylers: [
                    { visibility: 'off' },
                ]
            },
            {
                featureType: 'administrative',
                elementType: 'geometry.fill',
                stylers: [
                    { color: '#f2f2f2' },
                ]
            },
            {
                featureType: 'landscape',
                elementType: 'geometry',
                stylers: [
                    { color: '#f2f2f2' }
                ]
            },
            {
                featureType: 'landscape',
                elementType: 'labels',
                stylers: [
                    { visibility: 'off' }
                ]
            },
            {
                featureType: 'poi',
                elementType: 'geometry',
                stylers: [
                    { color: '#f2f2f2' }
                ],
            },
            {
                featureType: 'poi',
                elementType: 'labels',
                stylers: [
                    { visibility: 'off' }
                ]
            },
            {
                featureType: 'road',
                elementType: 'labels',
                stylers: [
                    { visibility: 'off' }
                ]
            },
            {
                featureType: 'road',
                elementType: 'highway',
                stylers: [
                    { color: '#bfbfbf' }
                ]
            },
            {
                featureType: 'water',
                elementType: 'geometry.fill',
                stylers: [
                    { color: '#ccdbeb' }
                ]
            }
        ];
    }
    JobLocationsComponent.prototype.ngOnInit = function () {
        // this.locationsService.getData('locations').subscribe(
        //   locationsArray => {
        //     this.locationsArray = locationsArray;
        //   }
        // );
    };
    JobLocationsComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: __webpack_require__(1112),
            styles: [__webpack_require__(1097)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__tse_service__["a" /* TseService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__tse_service__["a" /* TseService */]) === 'function' && _a) || Object])
    ], JobLocationsComponent);
    return JobLocationsComponent;
    var _a;
}());


/***/ },

/***/ 1078:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return JobPostingDataComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var JobPostingDataComponent = (function () {
    function JobPostingDataComponent() {
    }
    JobPostingDataComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: __webpack_require__(1113),
            styles: [__webpack_require__(1098)]
        }), 
        __metadata('design:paramtypes', [])
    ], JobPostingDataComponent);
    return JobPostingDataComponent;
}());


/***/ },

/***/ 1079:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return TopJobCategoriesComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TopJobCategoriesComponent = (function () {
    function TopJobCategoriesComponent() {
    }
    TopJobCategoriesComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: __webpack_require__(1114),
            styles: [__webpack_require__(1099)]
        }), 
        __metadata('design:paramtypes', [])
    ], TopJobCategoriesComponent);
    return TopJobCategoriesComponent;
}());


/***/ },

/***/ 1091:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__career_site_jobs_component__ = __webpack_require__(1076);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__job_locations_job_locations_component__ = __webpack_require__(1077);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__job_posting_data_job_posting_data_component__ = __webpack_require__(1078);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__top_job_categories_top_job_categories_component__ = __webpack_require__(1079);
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return routing; });





var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_1__career_site_jobs_component__["a" /* CareerSiteJobsComponent */],
        children: [
            {
                path: '',
                redirectTo: 'job-locations',
                pathMatch: 'full'
            },
            {
                path: 'job-locations',
                component: __WEBPACK_IMPORTED_MODULE_2__job_locations_job_locations_component__["a" /* JobLocationsComponent */]
            },
            {
                path: 'job-posting-data',
                component: __WEBPACK_IMPORTED_MODULE_3__job_posting_data_job_posting_data_component__["a" /* JobPostingDataComponent */]
            },
            {
                path: 'top-job-categories',
                component: __WEBPACK_IMPORTED_MODULE_4__top_job_categories_top_job_categories_component__["a" /* TopJobCategoriesComponent */]
            }
        ]
    }
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* RouterModule */].forChild(routes);


/***/ },

/***/ 1096:
/***/ function(module, exports) {

module.exports = "/* Navigation Tabs */\n.summary-tab {\n  width: 20%;\n  float: left;\n  height: 145px;\n  background-color: #f2f2f2;\n  border-right: 2px solid #e0e2e4;\n  border-top: 5px solid #c6ddec;\n  text-align: center; }\n\n.summary-tab.selected {\n  border-top-color: #1873ba;\n  background-color: #fff; }\n\n.summary-tab:hover {\n  background-color: #fff;\n  border-top-color: #1873ba;\n  cursor: pointer; }\n\n.summary-tab h2 {\n  color: #5999c9;\n  font-family: frutiger47, arial, sans-serif;\n  font-size: 60px;\n  margin-top: 15px; }\n\n.summary-tab:hover > h2, .summary-tab.selected > h2 {\n  color: #1873ba;\n  font-size: 60px;\n  font-family: frutiger77, arial, sans-serif; }\n\n.summary-tab h4 {\n  font-family: frutiger57, arial, sans-serif;\n  font-size: 16px;\n  color: #5999c9; }\n\n.summary-tab:hover > h4, .summary-tab.selected > h4 {\n  color: #1873ba; }\n\n.jobs > div, .visits > div, .members > div, .emails > div, .interests > div {\n  width: 44px;\n  height: 44px;\n  background: url(\"assets/images/summary-report-sprite.png\") no-repeat 0 0;\n  margin: -25px auto 0; }\n\n.jobs > div {\n  background-position: 0 -48px; }\n\n.visits > div {\n  background-position: -46px -50px; }\n\n.members > div {\n  background-position: -95px -50px; }\n\n.emails > div {\n  background-position: -141px -48px; }\n\n.interests > div {\n  background-position: -187px -48px; }\n\n.jobs:hover > div, .jobs.selected > div {\n  background-position: 0 0; }\n\n.visits:hover > div, .visits.selected > div {\n  background-position: -46px -1px; }\n\n.members:hover > div, .members.selected > div {\n  background-position: -95px -1px; }\n\n.emails:hover > div, .emails.selected > div {\n  background-position: -141px -1px; }\n\n.interests:hover > div, .interests.selected > div {\n  background-position: -187px -1px; }\n\n/* start the jobs slider within Career Site Jobs tab */\n.jobs-slider-wrap {\n  margin: 0 auto;\n  width: 342px;\n  /*display: flex;\n\talign-items: center;\n\tjustify-content: center;*/ }\n\n.jobs-slider-wrap ul {\n  margin-top: 25px;\n  display: block;\n  margin-left: auto;\n  margin-right: auto; }\n\n.jobs-slider-wrap ul li {\n  float: left;\n  font-size: 15px;\n  color: #6E6E71;\n  opacity: .7;\n  font-family: frutiger57, arial, sans-serif;\n  cursor: pointer;\n  /*border-bottom: 3px solid #6e6e71;*/ }\n\n.jobs-slider-wrap ul li.active {\n  color: #1b75bb;\n  opacity: 1;\n  /*font-family: frutiger77, arial, sans-serif;*/\n  font-weight: bold;\n  /*border-bottom: 3px solid #1b75bb;*/ }\n\n.jobs-slider-wrap ul li:nth-child(2) {\n  padding: 0 30px; }\n\n.slider-bar {\n  width: 100%;\n  margin-top: 2px;\n  height: 2px;\n  background-color: #d4d4d7;\n  position: relative; }\n\n.slider {\n  height: 3px;\n  position: absolute;\n  left: 0px;\n  top: -1px;\n  width: 80px;\n  background-color: #1b75bb; }\n"

/***/ },

/***/ 1097:
/***/ function(module, exports) {

module.exports = ".sebm-google-map-container {\n  height: 500px; }\n\nsebm-google-map div.gm-style div:first-child div:nth-child(3) div:nth-child(4) div:first-child div:first-child div:nth-child(3) {\n  display: none !important; }\n"

/***/ },

/***/ 1098:
/***/ function(module, exports) {

module.exports = "body {\n  background: #fff; }\n\nsvg {\n  shape-rendering: crispEdges; }\n\nsvg text {\n  font-family: arial, sans-serif;\n  font-weight: 700; }\n\n.tick text {\n  font-size: 13px;\n  font-family: frutiger57, arial, sans-serif;\n  font-weight: normal;\n  stroke-width: .8px;\n  fill: #808285; }\n\n.xAxis line, .yAxis line {\n  stroke: #aaa;\n  shape-rendering: crispEdges; }\n\n.grid .tick {\n  stroke: #ccc;\n  opacity: 0 !important; }\n\n.graph-left-nav {\n  width: 21%;\n  margin-top: -25px; }\n\n.job-search-container {\n  background-color: #eee;\n  border: 1px solid #ccc;\n  border-radius: 5px;\n  width: 93%;\n  overflow: auto; }\n\n.fa-search {\n  color: #6E6E71;\n  opacity: .7; }\n\n.search-icon {\n  float: left;\n  padding: 4px;\n  width: 22px; }\n\n.job-search-container input {\n  border: none;\n  background-color: #eee;\n  float: left;\n  width: 166px;\n  padding: 0; }\n\n.graph-left-nav ul {\n  margin-top: 7px;\n  font-family: frutiger57, arial, sans-serif;\n  color: #1b75bb;\n  font-size: 14px; }\n\n.graph-left-nav ul li {\n  padding: 9px 0 4px 13px;\n  border-top: 1px solid #d4d4d7; }\n\n.graph-left-nav ul li:hover {\n  background-color: #dfedfa; }\n\n.graph-left-nav ul li.selected {\n  background-color: #dfedfa; }\n\n.graph-container {\n  width: 79%; }\n\n.graph-container h3 {\n  padding-left: 55px;\n  padding-top: 30px;\n  color: #1873ba;\n  font-family: frutiger57, arial, sans-serif;\n  font-size: 30px;\n  letter-spacing: 1px; }\n"

/***/ },

/***/ 1099:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 1111:
/***/ function(module, exports) {

module.exports = "    <!-- this is the slider within the Career Site Jobs tab -->\n    <div class=\"container pt20\" id=\"body_container\">\n\n        <div class=\"col-xs-12\">\n            <div class=\"row summary-tab-wrap\" style=\"margin-top: 35px;background-color: #fff\">\n\n                <div routerLink=\"/career-site-jobs\" routerLinkActive=\"selected\" class=\"summary-tab jobs\">\n                    <div></div>\n                    <h2>601</h2>\n                    <h4>Career Site Jobs</h4>\n                </div>\n\n                <div routerLink=\"/career-site-visits\" routerLinkActive=\"selected\" class=\"summary-tab visits\">\n                    <div></div>\n                    <h2>70,000</h2>\n                    <h4>Career Site Visits</h4>\n                </div>\n\n                <div routerLink=\"/members\" routerLinkActive=\"selected\" class=\"summary-tab members\">\n                    <div></div>\n                    <h2>10,000</h2>\n                    <h4>Members</h4>\n                </div>\n\n                <div routerLink=\"/emails\" routerLinkActive=\"selected\" class=\"summary-tab emails\">\n                    <div></div>\n                    <h2>1,026</h2>\n                    <h4>Emails</h4>\n                </div>\n\n                <div routerLink=\"/expressions-of-interest\" routerLinkActive=\"selected\" class=\"summary-tab interests\">\n                    <div></div>\n                    <h2>329</h2>\n                    <h4>Expressions of Interest</h4>\n                </div>\n\n            </div> <!-- [end] .row -->\n            <div class=\"row jobs-slider-wrap\">\n                <ul>\n                    <li routerLink=\"job-locations\" routerLinkActive=\"active\" class=\"locations\" (click)=\"moveSlider(whichReport)\">Job Locations</li>\n                    <li routerLink=\"job-posting-data\" routerLinkActive=\"active\" class=\"posting-data\" (click)=\"moveSlider()\">Job Posting Data</li>\n                    <li routerLink=\"top-job-categories\" routerLinkActive=\"active\" class=\"categories\" (click)=\"moveSlider()\">Top Job Categories</li>\n                </ul>\n                <div class=\"clear\"></div>\n                <div class=\"slider-bar\">\n                    <div [@reportState]=\"whichReport\" class=\"slider\"></div>\n                </div>\n\n            </div> <!-- [end] .row -->\n            <div class=\"row\">\n              <router-outlet></router-outlet>\n            </div>\n        </div> <!-- [end] .col-xs-12 -->\n"

/***/ },

/***/ 1112:
/***/ function(module, exports) {

module.exports = "<sebm-google-map [styles]=\"stylesArray\" [zoomControl]=\"false\" [zoom]=\"2\" [scrollwheel]=\"false\" [mapDraggable]=\"false\" [streetViewControl]=\"false\" [latitude]=\"lat\" [longitude]=\"lng\">\n  <sebm-google-map-info-window [latitude]=\"60\" [longitude]=\"140\" [isOpen]=\"true\" [disableAutoPan]=\"true\">\n    Hi, this is the content of the <strong>info window</strong>\n  </sebm-google-map-info-window>\n  <sebm-google-map-marker *ngFor=\"let coord of locationsArray\" [latitude]=\"coord.lat\" [longitude]=\"coord.lng\"></sebm-google-map-marker>\n</sebm-google-map>\n"

/***/ },

/***/ 1113:
/***/ function(module, exports) {

module.exports = "<div class=\"col-xs-12\" >\n        <div class=\"row\" style=\"background-color:white\">\n            <div class=\"col-xs-3 graph-left-nav\">\n                \n                    <div class=\"fl\">Job Category</div>\n                    <div class=\"fl\">Location</div>\n                    <div class=\"clear\"></div>\n                \n                <div class=\"job-search-container\">\n                    <div class=\"search-icon\">\n                        <i class=\"fa fa-search\"></i>\n                    </div>\n                    <input type=\"text\" placeholder=\"Search job category\"/>\n                </div>\n                <div class=\"clear\"></div>\n                <div class=\"mCustomScrollbar\" style=\"height: 450px\">\n                    <ul id=\"data_list\">\n                        <li onclick=\"reloadData();\">Operational Managers</li>\n                        <li>Product Managers</li>\n                        <li>Budget Analysis</li>\n                        <li>Financial Analysis</li>\n                        <li>Accounting and Legal</li>\n                        <li>Computer Programmer</li>\n                        <li>Design and Media</li>\n                        <li>Computer Systems Database</li>\n                        <li>Customer Service and Training</li>\n                        <li>Chemical Engineers</li>\n                        <li>Lawyers</li>\n                        <li>Account Clerks</li>\n                        <li>Sales Representative</li>\n                        <li>Sales Representative</li>\n                        <li>Sales Representative</li>\n                        <li>Sales Representative</li>\n                    </ul>\n                </div>\n                \n            </div>\n            <div class=\"col-xs-9 graph-container\">\n                <h3>Job Posting Volume - All Job Categories</h3>\n                \n                <svg id=\"svg\"></svg>\n                    \n            </div>\n        </div>\n    </div>\n"

/***/ },

/***/ 1114:
/***/ function(module, exports) {

module.exports = "<h3>Top Job Categories Works!</h3>\n"

/***/ }

});
//# sourceMappingURL=1.map